<?php

/**
 * Fetcher backups assumes that you will have a local strategy for
 * creating a backup locally on that server and that you will then
 * have a separate strategy for synchronzing the backups on that server
 * with another location.  This makes sesnse for git as a backup tool
 * but may not make sense for other backup strategies.
 *
 * Patches welcome.
 */

/**
 * Implements hook_fetcher_search_paths().
 */
function fetcher_backups_fetcher_search_paths() {
  return array(
    // Classes in this module install.
    __DIR__ . '/lib/',
  );
}

/**
 * Implements hook_drush_command().
 */
function fetcher_backups_drush_command() {
  $items = array();
  $items['fetcher-backup-directory'] = array(
    'description' => 'Back up a folder on disk',
    'arguments' => array(
      'source' => 'The folder that you intend to backup.',
      'destination' => 'Another location on disk where the backups should be placed.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $options = array(
    'backup-dirs' => 'Add additional directories within the site\'s working directory to backup (in addition to the SQL backup and private and public files) as relative paths from the working directory separated by commas (commas escaped with `\`).',
    'exclude-database' => 'Do not create a database backup',
    'exclude-files' => 'Do not backup any files.',
    'exclude-private-files' => 'Do not backup private files.',
    'exclude-public-files' => 'Do not backup public files.',
  );
  $items['fetcher-backup-site'] = array(
    'description' => 'Backup the public and private files as well as the database for a site.',
    'arguments' => array(
      'site' => 'The fetcher name of the site on this server.',
    ),
    'options' => $options,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  $items['fetcher-backup-sites-on-host'] = array(
    'description' => 'Run backups for all sites on a given hostname.',
    'options' => array(
      'server-hostname' => 'If set explicity, used as the hostname to find the list of site environments on this server. This should normally be left blank.',
    ) + $options,
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

/**
 * Backup a directory from one place to another.
 *
 * @param $source
 *   The directory to backup.
 * @param $destination
 *   The directory to backup to.
 */
function drush_fetcher_backups_fetcher_backup_directory($source, $destination) {
  $source = fetcher_backups_find_path($source);
  $destination = fetcher_backups_find_path($destination);
  $backupHandler = fetcher_backups_get_backup_handler();
  try {
    $backupHandler->backupFolder($source, $destination);
  }
  catch (Exception $e) {
    fetcher_backups_drush_exception_handler($e, dt('Backup failed from %source to %destination', array('%source' => $source, '%destination' => $destination))); 
  }
}

/**
 * Try to find the "intended path" for the folder specified.
 *
 * @param $path
 *   The path which may be relative or start with `~` for the user's home folder.
 * @return string
 *   The derived path.
 */
function fetcher_backups_find_path($path) {
  // We assume path is in the home folder.
  if (strpos($path, '~') === 0) {
    $path = drush_fetcher_get_site()->site['system']->getUserHomeFolder() . '/' . substr($path, 1);
  }
  // We assume path is relative.
  else if (strpos($path, '/') !== 0) {
    $path = getcwd() . '/' . $path;
  }
  // We assume it's absolute.
  drush_print($path);
  return $path;
}

/**
 * Backup a fetcher site including both files and database.
 */
function drush_fetcher_backups_fetcher_backup_site($site_name = FALSE) {
  $site = fetcher_get_site($site_name);
  if (!$site) {
    drush_set_error('FETCHER_BACKUP_SITE_NOT_FOUND', dt('The site was not found.'));
    return;
  }
  $backup_dirs = fetcher_backups_get_backup_dirs_from_options();
  try {
    drush_fetcher_backup_site($site, $backup_dirs);
  }
  catch (Exception $e) {
    fetcher_backups_drush_exception_handler($e, dt('Site backup failed for site %site.', array('%site' => $site['name']))); 
  }
}

/**
 * Backup all sites on a host.
 */
function fetcher_backups_backup_fetcher_sites() {
  $sites = drush_fetcher_get_sites_for_host($hostname);
  $backup_dirs = fetcher_backups_get_backup_dirs_from_options();
  $errors = array();
  if (!empty($sites)) {
    foreach ($sites as $site) {
      try {
        drush_fetcher_backup_site($site, $backup_dirs);
      }
      catch (Exception $e) {
        $errors[] = $site['name'] . '.' . $site['environment.local'];
      }
    }
    if (count($errors)) {
      $args = array('@environments' => implode(', ', $errors));
      drush_set_error('fetcher_backups', dt('Error performing backups on the following site environments: @environments', $args));
    }
  }
}

/**
 * Builds a list of backup dirs from the global drush options.
 */
function fetcher_backups_get_backup_dirs_from_options() {
  // `$backup_dirs` should be a linear array of directories to be backed up as
  // paths relative to the working directory of `$site`.
  $backup_dirs = fetcher_backups_parse_backup_dirs(drush_get_option('backup-dirs', ''));
  if (!drush_get_option('exclude-files')) {
    if (!drush_get_option('exclude-private-files')) {
      $backup_dirs[] = 'public_files';
    }
    if (!drush_get_option('exclude-public-files')) {
      $backup_dirs[] = 'private_files';
    }
  }
  return $backup_dirs;
}

/**
 * Run backups for all sites on this hostname.
 */
function drush_fetcher_backup_sites_on_host($hostname = NULL) {
  $hostname = drush_get_option('server-hostname');
  $sites = drush_fetcher_get_sites_for_host($hostname);
  $failures = array();
  foreach ($sites as $site) {
    try {
      drush_log("Now executing: `drush cron --root={$site['site.webroot']} --uri={$site['hostname']}`", 'info');
      drush_fetcher_backup_site($site, $backup_dirs);
      drush_set_error('CRON_FAILURE', dt('Error running cron for sites: @sites', array('@sites' => implode($failures, ', '))));
    }
    catch (Exception $e) {
      fetcher_backups_drush_exception_handler($e, dt('Site backup failed for site %site.', array('%site' => $site['name']))); 
      $failures[] = $site['name'] . ':' . $site['environment.local'];
    }
  }
  if (count($failures)) {
    drush_set_error('CRON_FAILURE', dt('Error running cron for sites: @sites', array('@sites' => implode($failures, ', '))));
  }
}

/**
 * Backup a site.
 *
 * This is an API function to back up a fetcher site.
 */
function drush_fetcher_backup_site(\Fetcher\Site $site, $backup_dirs = array()) {
  $backup_handler = fetcher_backups_get_site_backup_handler($site);
  if (!drush_get_option('exclude-database')) {
    $backup_dirs[] = $site['fetcher_backup_db_folder'];
    $backup_handler->backupDatabase();
  }
  $site['fetcher_backup_site_folders'] = $backup_dirs;
  $backup_handler->backupDirectories();
}


/**
 * Allow commas to be used in folders to be backed up by fetcher.
 */
function fetcher_backups_parse_backup_dirs($backup_dirs) {
  if ($backup_dirs == '') {
    return array();
  }
  str_replace('\,', '{FETCHERBACKUPDIRCOMMA}', $backup_dirs);
  $backup_dirs = explode(',', $backup_dirs);
  $function = function($value) {
    return str_replace('{FETCHERBACKUPDIRCOMMA}', ',', $value);
  };
  return array_map($function, $backup_dirs);
}

/**
 * Factory function for the site backup class.
 */
function fetcher_backups_get_site_backup_handler(\Fetcher\SiteInterface $site) {
  $class = $site['fetcher-site-backup.class'];
  return new $class($site);
}

/**
 * Retrieves a configured backup handler.
 */
function fetcher_backups_get_backup_handler() {
  $class = fetcher_drush_get_option('fetcher_backups_directory_class', 'FetcherBackups\FilesBackup\GitFolderBackup');
  return new $class(fetcher_get_site());
}

/**
 * Handle exceptions for our commands in a generic way.
 *
 * @param $exception
 *   The exception thrown.
 * @param $message
 *   The translated message to be displayed in addition to the exception output.
 */
function fetcher_backups_drush_exception_handler(Exception $e, $message) {
  $args = array(
    '@error' => $e->getMessage(),
    '@file' => $e->getFile(),
    '@line' => $e->getLine(),
  );
  drush_log(dt('@error in file @file on line @line.', $args), 'error');
  drush_log($e->getTraceAsString(), 'debug');
  drush_set_error('FETCHER_BACKUP_FAILURE', $message);
}
