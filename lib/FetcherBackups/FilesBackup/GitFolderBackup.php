<?php

namespace FetcherBackups\FilesBackup;
use Symfony\Component\Process\Process;

class GitFolderBackup {

  private $site = FALSE;

  public function __construct(\Fetcher\Site $site) {
    $this->site = $site;
    if (!isset($site['fetcher_backup_commit_message'])) {
      $site['fetcher_backup_commit_message'] = function($c) {
        return escapeshellarg('Automated commit ' . date('d/m/Y G:i:s'));
      };
    }
    if (!isset($site['fetcher_backup_commit_author'])) {
      $site['fetcher_backup_commit_author'] = function($c) {
        return sprintf('Fetcher Backups <fetcher@%s>', $c['system hostname']);
      };
    }
  }

  /**
   * Backup a folder from a source to a backup location.
   *
   * @param $source
   *   A string representing the path of the folder to backup from (relative or absolute).
   * @param $destination
   *   A string representing the path of the folder to backup to (relative or absolute).
   */
  public function backupFolder($source, $destination) {
    $this->ensureGitRepo($source);
    $this->site['system']->ensureFolderExists($destination);
    $this->ensureGitRepo($destination, TRUE);
    if (!$this->commitAllChanges($source)) {
      throw new \Exception('Unkown git commit error!');
    }
    if (!$this->backupToRemoteFolder($source, $destination)) {
      throw new \Exception('Git push backup syncronization failed!');
    }
  }

  /**
   * Ensure that a folder is a reposiotry.
   */
  public function ensureGitRepo($directory, $bare = FALSE) {
    if (!$this->isGitFolder($directory)) {
      $this->initGitRepo($directory, $bare);
    }
  }

  /**
   * Checks whether a folder is a git repository.
   */
  public function isGitFolder($directory) {
    $return = FALSE;
    if ($this->executeGitCommand('status', $directory)->isSuccessful()) {
      if (is_dir($directory . '/.git') || is_dir($directory . '/refs')) {
        $return = TRUE;
      }
    }
    return $return;
  }

  /**
   * initialize a git repository in a folder.
   */
  public function initGitRepo($directory, $bare = FALSE) {
    $command = $bare ? 'init --bare' : 'init';
    return $this->executeGitCommand($command, $directory)->isSuccessful();
  }

  /**
   * If there are any changes in the folder, commit them.
   */
  public function commitAllChanges($directory) {

    $status = $this->executeGitCommand('status', $directory);
    if (!$status->isSuccessful()) {
      throw new \Exception(sprintf('Unknown git error with output: %s.', $status->getErrorOutput()));
    }

    $add = $this->executeGitCommand('add -A', $directory)->isSuccessful();
    $diff = $this->executeGitCommand('diff --cached --stat', $directory)->getOutput();
    if ($add && !empty($diff)) {
      $command = sprintf('commit --author="%s" -m %s', $this->site['fetcher_backup_commit_author'], $this->site['fetcher_backup_commit_message']);
      $commit = $this->executeGitCommand($command, $directory);
      if (!$commit->isSuccessful()) {
        throw new \Exception(sprintf('Unknown git commit error with output: %s.', $status->getErrorOutput()));
      }
    }
    return TRUE;
  }

  /**
   * Backup to a remote directory.
   */
  public function backupToRemoteFolder($source, $destination) {
    // TODO: Check to see if there are commits to push.
    //drush_print(sprintf('"%s"', $this->executeGitCommand('log', $source)->getOutput()));
    // TODO: Make the branch configurable?
    return $this->executeGitCommand('push ' . $destination . ' master', $source)->isSuccessful();
  }

  /**
   * Execute a git command using the configured binary.
   *
   * @param $command
   *   The command (sans `git` binary)
   * @param $cwd
   *   The directory to cwd to before running the commenad.
   * @return
   *   The process object.
   *
   * TODO: Somehow merge this into the code fetcher class? Or inherit?
   */
  public function executeGitCommand() {
    $args = func_get_args();
    $site = $this->site;

    // By default, allow git to be located automatically within the include path.
    $gitBinary = 'git';
    // If an alternate binary path is specified, use it.
    if (isset($site['git binary'])) {
      $gitBinary = $site['git binary'];
    }
    $cwd = null;
    if (isset($args[1])) {
      $cwd = $args[1];
    }
    $args[0] = $gitBinary . ' ' . $args[0];
    $command = call_user_func_array('sprintf', $args);
    $site['log'](sprintf('Executing `%s` from folder `%s`.', $command, $cwd));

    // Attempt to ramp up the memory limit and execution time
    // to ensure big or slow chekcouts are not interrupted, storing
    // the current values so they may be restored.
    $timeLimit = ini_get('max_execution_time');
    ini_set('memory_limit', -1);
    $memoryLimit = ini_get('memory_limit');
    ini_set('max_execution_time', 0);

    $process = new Process($command, $cwd);
    if (!$site['simulate']) {
      // Git operations can run long, set our timeout to an hour.
      $process->setTimeout(3600);
      $process->run();
    }

    // Restore the memory limit and execution time.
    ini_set('memory_limit', $memoryLimit);
    ini_set('max_execution_time', $timeLimit);

    return $process;
  }
}
