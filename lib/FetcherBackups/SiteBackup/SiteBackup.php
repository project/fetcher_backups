<?php

namespace FetcherBackups\SiteBackup;
use Symfony\Component\Process\Process;
use FetcherBackups\SiteBackup\SiteBackupInterface;

class SiteBackup implements SiteBackupInterface {

  private $site;

  public function __construct(\Fetcher\Site $site) {

    $site->setDefaultConfigration('mysqldump-binary', 'mysqldump');
    $site->setDefaultConfigration('fetcher-backups.site-directory', '/var/lib/fetcher-backups');
    $site->setDefaultConfigration('fetcher_backup_directory', function($c) {
      return escapeshellarg('Automated commit ' . date('d/m/Y G:i:s'));
    });
    $site->setDefaultConfigration('fetcher_backup_db_folder', 'database_backup');
    $site->setDefaultConfigration('fetcher-backup.database.backup_directory', function($c) {
      return $c['site.working_directory'] . '/' . $c['fetcher_backup_db_folder'];
    });
    $site->setDefaultConfigration('fetcher_backup_site_folders', function($c) {
      return array(
        'public_files',
        'private_files',
        $c['fetcher_backup_db_folder'],
      );
    });
    $site->setDefaultConfigration('fetcher-backup.gitignore', array(
      'database_backup' => array(
        '# Git ignore added by fetcher backups',
        'cache*.txt',
        'session.txt',
        'ctools_css_cache.txt',
        'ctools_object_cache.txt',
      ),
      'private_files' => array(
        '# Git ignore added by fetcher backups',
      ),
      'public_files' => array(
        '# Git ignore added by fetcher backups',
        'css/*',
        'ctools/*',
        'js/*',
        'imagecache/*',
        'styles/*',
        'imagefield_thumbs/*',
      ),
    ));

    $this->site = $site;
  }

  /**
   * Backups the database and files for a site.
   */
  public function backupSite() {
    $this->backupDatabase();
    $this->backupDirectories();
  }

  /**
   * Backup the configured directories to the configured destiation.
   */
  public function backupDirectories() {
    foreach ($this->site['fetcher_backup_site_folders'] as $name) {
      if (is_dir($this->site['site.working_directory'] . '/' . $name)) {
        $this->backupSiteFolder($name);
      }
    }
  }

  /**
   * Backup the site database to the configured directory as a tab based backup.
   */
  public function backupDatabase() {
    $site = $this->site;
    $site['system']->ensureFolderExists($site['fetcher-backup.database.backup_directory'], NULL, 'mysql');
    // Clear out the backup working directory to prevent errors.
    $rm = $site['process'](sprintf('rm -f %s/*.txt %s/*.sql', $site['fetcher-backup.database.backup_directory'], $site['fetcher-backup.database.backup_directory']));
    $rm->run();
    if (!$rm->isSuccessful()) {
      $site['log'](sprintf('Command output: %s', $rm->getErrorOutput()), 'debug');
      throw new \Exception('Clearing the database backup directory failed.');
    }
    $ignore = $site['fetcher-backup.gitignore'];
    $command = sprintf('%s --single-transaction --lines-terminated-by=0x0d0a %s %s', $site['mysqldump-binary'], escapeshellarg('--tab=' . $site['fetcher-backup.database.backup_directory']), $site['database.database']);
    $site['log'](sprintf('Executing: `%s`', $command), 'info');
    $process = $site['process']($command);
    $process->run();
    if (!$process->isSuccessful()) {
      throw new \Exception(sprintf('MySQLdump failed with output: %s', $process->getErrorOutput()));
    }
  }

  /**
   * Backup a folder within the sites working directory.
   *
   * @param $name
   *   The name of the folder within the site directory.
   */
  public function backupSiteFolder($name) {
    $site = $this->site;
    $class = $site['fetcher-backups.directory-class'];
    $handler = new $class($site);
    $destination = $site['fetcher-backups.site-directory'] . '/' . $site['name'] . '.' . $site['environment.local'] . '-' . $name;
    $this->site['system']->ensureFolderExists($destination);
    $source = $site['site.working_directory'] . '/' . $name;
    $ignore = $site['fetcher-backup.gitignore'];
    $content = implode(PHP_EOL, $ignore[$name]);
    $site['system']->writeFile($source . '/.gitignore', $content);
    $handler->backupFolder($source, $destination);
  }
}
